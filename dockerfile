FROM alpine:3.14 as builder
ARG VERSION=5.2.0
RUN apk add --no-cache bc cargo gcc libffi-dev musl-dev openssl-dev py3-pip python3 python3-dev rust \
    && python3 -m venv /opt/venv \
    && source /opt/venv/bin/activate \
    && pip3 install --no-cache-dir --no-compile ansible-lint==$VERSION yamllint \
    && pip3 install ansible

FROM alpine:3.14 as default

RUN apk add --no-cache git python3

COPY --from=builder /opt/venv /opt/venv

# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /src
ENTRYPOINT ["ansible-lint", "-p", "-v"]
CMD ["--version"]
