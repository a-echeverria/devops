provider "aws" {
  region = "us-east-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_key_pair" "my_key" {
  key_name   = "Theoo"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCk2nrKGvv4hrnHMBI7BxZoeJsOKxzencyw3ecoXKPTXeimcSenonBvTSuIaAvDpd+oiWSeQaZFVu6rC9HjHVzWunEBix4O3o4t5VHZp7N0l939Vr9WJAgMBv1ZObe0pdbVN+njqWQrSycCEOuSxDC5fkZ5+L151/TnZpwlFVhNJXdMqJ4Gxw10OkLHQNaEFvVq5wDOe8DsPod/i1xPuK4OOY3fCMhs61wkRPMilxg6OxzqbQ4wt2Zz5TJItN5ENEeQZVue/l9GBQgE57cguJMDH+W5/xx5agxBq0rSX9aJ0fQdCaCS5pvIRKpJp6iQPNlVHgaIuLhzCTNSi7omtcwRFJAWnvJRhF4Rb7VUEOHyIE2sYpnhD1Vf2h08To0lmyVAW8yItc4Rlz+AqRPclyJKQVf0zC96GIPr3V0+/TNY4isJMEsSjhUHgeZAU/IThYTRax+ToPgtO9/EVDbe6nRUyWucMy0tywiFB2VSUKr6toDyI0XKQCvIGJWJnoEtRgFucZal0KVyR9pOZZdehJd1fVaOGink8yuAcBQ1z3DrAYXnQd6ST5kLDVvHMB1TBb/szADIRwL09gemAoWMUTQsP+tYClfXoeDnPSAeuB7hXR4wsUt/Quy+oQmpWLlxtJRtg1e/xr+cw8w7/YZzR9BMxuE2q+LpNELlE+/pg2Y8zw== nashad@MacBook-Pro-de-Nashad.local"

}
//
resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "my_ig" {
  vpc_id = aws_vpc.my_vpc.id
}

resource "aws_route_table" "example" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_ig.id
  }
}

resource "aws_subnet" "my_subnet" {
  availability_zone = "us-east-1a"
  cidr_block        = "10.0.221.0/24"
  vpc_id            = aws_vpc.my_vpc.id
}

resource "aws_route_table_association" "my_route_table" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.example.id
}

resource "aws_instance" "web" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.my_key.key_name
  subnet_id                   = aws_subnet.my_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.my_sg.id]

  tags = {
    Name = "Theo"
  }
}

resource "aws_security_group" "my_sg" {
  name        = "allow SSH"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.my_vpc.id
}

resource "aws_security_group_rule" "my_sg_rule" {
  security_group_id = aws_security_group.my_sg.id

  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-up-and-running-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

terraform {
  backend "s3" {
    bucket         = "groupe1-5iw1-2022-esgi-bucket-devops"
    key            = "global/s3/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-up-and-running-locks"
    encrypt        = true
  }
}

output "ip" {
  value = aws_instance.web.public_ip
}